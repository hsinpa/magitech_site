import React from "react";
import { Element  } from 'react-scroll';

export default class Home extends React.Component {

  componentDidMount() {
    $(".various").fancybox({
  		maxWidth	: 800,
  		maxHeight	: 600,
  		fitToView	: false,
  		width		: '80%',
  		height		: '70%',
  		autoSize	: false,
  		closeClick	: false,
  		openEffect	: 'none',
  		closeEffect	: 'none'
  	});
  }


  render() {
    return (
      <div id="wrainbo-home-headboard">
        <section class="row">
          <img src="image/icon/wrainbo_icon.png" />
            <div class="row">
              <h1>Play to learn business analytics</h1>
              <div class="breakline"></div>
              <h5>A mobile business learning game</h5>
              <a class="various fancybox.iframe" href="https://www.youtube.com/embed/1HWUZnZxYyA?autoplay=1">WATCH</a>
            </div>
        </section>
      </div>
    );
  }
}
