import React from "react";
import{ Element  } from 'react-scroll';
import AboutUsDetail from "./AboutUsDetail";

import{ Scroll, Link as Scroll_Link } from 'react-scroll';

export default class AboutUs extends React.Component {

  render() {

    return (
      <article id="wrainbo-aboutus">
        <div id="wrainbo-aboutus-main">

        <img class="aboutUsMainIcon" src="image/aboutus/ic-wrainbo.png"></img>

        <h1>Make learning engaging and effective</h1>

          <Scroll_Link to="detail" spy={true} smooth={true} duration={500}>
            <img src="image/aboutus/bt-seehow.png"></img>
          </Scroll_Link>
        </div>

        <Element name="detail">
          <AboutUsDetail />
        </Element>

      </article>
    );
  }
}
