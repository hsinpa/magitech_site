import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Layout from "./view/layout/Layout";
import Home from "./view/home/Home";
import AboutUs from "./view/aboutUs/AboutUs";
import Learning from "./view/learning/Learning";
import GamePlay from "./view/gameplay/GamePlay";

require("../css/main/app.scss");


const app = document.getElementById('app');
ReactDOM.render((
  <Router history={hashHistory}
    onUpdate={function() {
        window.scrollTo(0, 0);
      }}>
    <Route path="/" name="home" component={Layout}>
      <IndexRoute component={Home} ></IndexRoute>
        <Route path="aboutUs" name="aboutUs" component={AboutUs}></Route>
        <Route path="gameplay" name="gameplay" component={GamePlay}></Route>
        <Route path="learning" name="learning" component={Learning}></Route>
    </Route>
  </Router>
), app)
